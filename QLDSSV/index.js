let dssv = [];
let BASE_URL = "https://62f8b7a13eab3503d1da1abc.mockapi.io";

let batLoading = () => {
  document.getElementById("loading").style.display = "flex";
};
let tatLoading = () => {
  document.getElementById("loading").style.display = "none";
};

let renderTable = (list) => {
  let contentHTML = "";
  list.forEach((item) => {
    let content = `<tr>
    <td>${item.ma}</td>
    <td>${item.name}</td>
    <td>${item.email}</td>
    <td>
    <img src="${item.image}" style="width:60px" alt="" />
    </td>
    <td>
    <button onclick = "xoaSV(${item.ma})" class = "btn btn-primary">Xoá</button>
    <button onclick = "suaSV(${item.ma})" class = "btn btn-warning">Sửa</button>
    </td>
    </tr>`;
    contentHTML += content;
  });
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
};

let renderDssv = () => {
  batLoading();
  axios({
    url: `${BASE_URL}/project`,
    method: "GET",
  })
    .then((res) => {
      tatLoading();
      console.log("res: ", res);
      let dssv = res.data;
      renderTable(dssv);
      console.log("dssv: ", dssv);
    })
    .catch((err) => {
      tatLoading();
      console.log("err: ", err);
    });
};
renderDssv();

let xoaSV = (id) => {
  console.log("id: ", id);
  batLoading();
  axios({
    url: `${BASE_URL}/project/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      tatLoading();
      console.log("res: ", res);
      renderDssv();
    })
    .catch((err) => {
      tatLoading();
      console.log("err: ", err);
    });
};

let themSV = () => {
  batLoading();
  let svMoi = layThongTinTuForm();
  console.log("svMoi: ", svMoi);
  axios({
    url: `${BASE_URL}/project`,
    method: "POST",
    data: svMoi,
  })
    .then((res) => {
      tatLoading();
      console.log("res: ", res);
      renderDssv();
    })
    .catch((err) => {
      tatLoading();
      console.log("err: ", err);
    });
};

let suaSV = (id) => {
  batLoading();
  axios({
    url: `${BASE_URL}/project/${id}`,
    method: "GET",
  })
    .then((res) => {
      tatLoading();
      console.log("res: ", res);
      showThongTinLenForm(res.data);
    })
    .catch((err) => {
      tatLoading();
      console.log("err: ", err);
    });
};

let capNhatSV = () => {
  let updateSV = layThongTinTuForm();
  axios({
    url: `${BASE_URL}/project/${updateSV.ma}`,
    method: "PUT",
    data: updateSV,
  })
    .then((res) => {
      console.log("res: ", res);
      renderDssv();
    })
    .catch((err) => {
      console.log("err: ", err);
    });
};

let resetSV = () => {
  document.getElementById("formQLSV").reset();
};
