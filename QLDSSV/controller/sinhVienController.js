let layThongTinTuForm = () => {
  let maSV = document.getElementById("txtMaSV").value;
  let tenSV = document.getElementById("txtTenSV").value;
  let emailSV = document.getElementById("txtEmail").value;
  let hinhAnh = document.getElementById("txtImg").value;

  return {
    name: tenSV,
    ma: maSV,
    email: emailSV,
    image: hinhAnh,
  };
};

let showThongTinLenForm = (item) => {
  document.getElementById("txtMaSV").value = item.ma;
  document.getElementById("txtTenSV").value = item.name;
  document.getElementById("txtEmail").value = item.email;
  document.getElementById("txtImg").value = item.image;
};
